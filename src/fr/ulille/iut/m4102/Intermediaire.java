package fr.ulille.iut.m4102;

/**
 * Cette classe introduit un intermédiaire entre la classe utilisatrice et
 * l'implémentation du traitement des chaînes. Au début cette classe se contente
 * de logger ce qui se passe puis elle va évoluer pour accéder au service à
 * distance. Le comportement de cette classe est totalement transparent pour la
 * classe Utilisatrice qui au final utilise les mêmes méthodes que si elle
 * appelait directement la classe AlaChaine.
 */
public class Intermediaire implements AlaChaineInterface {
	private Client client;

	public Intermediaire(Client client) {
		this.client = client;
	}

	public int nombreMots(String chaine) {
		String req = "CALL:nombreMots:param[string,\"" + chaine + "\"]";

		return Integer.parseInt(parseVal(send(req)));
	}

	public String parseVal(String param) {
		String rawVal = param.substring(param.indexOf('[') + 1, param.indexOf(']')).split(",")[1];
		return rawVal.substring(1, rawVal.length() - 1);
	}

	private String send(String req) {
		System.out.println(req);

		String res = client.envoyer(req);

		System.out.println(res);

		return res;
	}

	public String asphyxie(String chaine) throws PasDAirException {
		String req = "CALL:asphyxie:param[string,\"" + chaine + "\"]";

		return parseVal(send(req));
	}

	public String leetSpeak(String chaine) {
		String req = "CALL:leetSpeak:param[string,\"" + chaine + "\"]";

		return parseVal(send(req));
	}

	public int compteChar(String chaine, char c) {
		String req = "CALL:compteChar:param[string,\"" + chaine + "\"]:param[char,\"" + c + "\"]";

		return Integer.parseInt(parseVal(send(req)));
	}

}
