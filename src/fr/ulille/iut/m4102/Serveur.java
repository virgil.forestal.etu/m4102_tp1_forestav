package fr.ulille.iut.m4102;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalTime;

/**
 * Cette classe démarre une socket serveur et
 * attend les connexions des clients.
 * Quand un client se connecte, elle délègue
 * le travail à la classe AccesService.
 */
public class Serveur {
	private ServerSocket serveurSocket = null;

	public Serveur(int port) {
		try {
			// Création de la Socket Serveur qui permettra d'attendre les connexions
			serveurSocket = new ServerSocket(port);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public void miseEnService() {
		Socket unClient = null;

		// Boucle d'attente des clients
		while (true ) {
			try {
				// accept() est bloquant. Quand on en sort, on a un nouveau
				// client avec une nouvelle instance de socket
				unClient = serveurSocket.accept();
				displayConnection(unClient.getInetAddress(), LocalTime.now());

				AccesService as = new AccesService(unClient);

				Thread t = new Thread(as);
				t.start();
				
				System.out.println();

			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
	}
	
	public void displayConnection(InetAddress source,LocalTime time) {
		StringBuilder str = new StringBuilder();
		
		str.append("[").append(time).append("]");
		str.append(" ").append("Connection de ").append(source);
		
		System.out.println(str);
	}

	// La classe doit être exécutée en passant le port serveur à utiliser en paramètre
	public static void main(String[] args) {
		Serveur serveur = new Serveur (Integer.parseInt(args[0]));

		serveur.miseEnService();
	}   
}
