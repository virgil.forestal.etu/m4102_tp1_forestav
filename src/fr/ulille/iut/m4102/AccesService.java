package fr.ulille.iut.m4102;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Cette classe se trouvera côté service Elle recevra une requête sous forme de
 * chaîne de caractère conforme à votre protocole. Elle transformera cette
 * requête en une invocation de méthode sur le service puis renverra le résultat
 * sous forme de chaîne de caractères.
 */
public class AccesService implements Runnable {
	private Socket client;
	private AlaChaine alc;

	public AccesService(Socket client) {
		alc = new AlaChaine();
		this.client = client;
	}

	public String traiteInvocation(String invocation) {
		StringBuilder resp = new StringBuilder("RETURN:[");
		String[] req = invocation.split(":");

		switch (req[1]) {
		case "nombreMots":
			resp.append("int,\"");
			resp.append(alc.nombreMots(parseVal(req[2])));
			break;

		case "asphyxie":
			String val;
			try {
				val = alc.asphyxie(parseVal(req[2]));
				resp.append("string,\"").append(val);
			} catch (PasDAirException e) {
				resp.append("exception,\"").append(e.getMessage());
			}
			break;

		case "leetSpeak":
			resp.append("string,\"");
			resp.append(alc.leetSpeak(parseVal(req[2])));
			break;

		case "compteChar":
			resp.append("string,\"");
			resp.append(alc.compteChar(parseVal(req[2]), parseVal(req[3]).charAt(0)));
			break;
		}

		resp.append("\"]");

		return resp.toString();
	}

	public String parseVal(String param) {
		String rawVal = param.substring(param.indexOf('[') + 1, param.indexOf(']')).split(",")[1];
		return rawVal.substring(1, rawVal.length() - 1);
	}

	public String traiteRequete(String req) {
		String type = req.split(":")[0];

		switch (type) {
		case "CALL":
			return traiteInvocation(req);
		default:
			return "ERROR";
		}
	}

	public void run() {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
			PrintWriter out = new PrintWriter(client.getOutputStream());

			String req;
			while ((req = in.readLine()) != null) {
				out.println("~$ " + traiteRequete(req));
				out.flush();
			}
			
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}
	}
}
